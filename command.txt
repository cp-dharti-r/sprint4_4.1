- install nodeJs and npm

sudo apt install npm
npm --version

sudo apt install nodejs
nodejs --version

- install nodemon

sudo npm install -g nodemon

- run node server

set this in package.json
"nodeServer": "nodemon index.js"

run : npm run nodeServer